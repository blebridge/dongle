/**
 * @file serial_commands.c
 * @brief      BLE Bridge serial interface implementation.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ble_gap.h>
#include "device.h"
#include "macros.h"
#include "serial.h"
#include "serial_commands.h"

/**
 * @addtogroup   Serial_Commands
 * @{
 */

/**
 * @defgroup   Serial_Commands_ic Serial Commands internal components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

/**
 * @brief      Macro to declare and define the gazillions of command functions.
 *
 * @param[in]  NAME  The name of the function. Will be prepended with 'cmd_'
 * @param[in]  args       Pointer to the command arguments.
 * @param[in]  arglen     The length of the command argument vector.
 * @param[out] p_err      Pointer to the returning error.
 * @param[out] p_confirm  Flag to set, whether confirmation should happen
 *                        immediately.
 *
 * @return     Nothing.
 */
#define SERIAL_CMD_DEF( NAME )     void cmd_##NAME(const uint8_t* args, const uint16_t arglen, uint8_t* const p_err, bool* const p_confirm)

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/*! Function declaration to connect to a device. */
static SERIAL_CMD_DEF(ble_connect);
/*! Function declaration to query device information. */
static SERIAL_CMD_DEF(ble_get_device);
/*! Function declaration to query the number of detected devices. */
static SERIAL_CMD_DEF(ble_get_scanned);
/*! Function declaration to scan for devices. */
static SERIAL_CMD_DEF(ble_scan);

/*! Function declaration to discover characteristic. */
static SERIAL_CMD_DEF(char_discover);
/*! Function declaration to query the discovered characteristics. */
static SERIAL_CMD_DEF(char_get);
/*! Function declaration to read a characteristic value. */
static SERIAL_CMD_DEF(char_read);
/*! Function declaration for subscribing to a characteristic. */
static SERIAL_CMD_DEF(char_subscribe);
/*! Function declaration to write a characteristic value. */
static SERIAL_CMD_DEF(char_write);

/*! Function declaration to query the device status. */
static SERIAL_CMD_DEF(dev_status);

/*! Function declaration to discover the services of a device. */
static SERIAL_CMD_DEF(serv_discover);
/*! Function declaration to get the discovered services of a device. */
static SERIAL_CMD_DEF(serv_get);

/* Private variables ---------------------------------------------------------*/

/**
 * @}
 */

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

uint32_t Command_Parse_BLE(const uint8_t* args, const uint16_t arglen, uint8_t* const p_err, bool* const p_confirm)
{
    switch( *args ) {
        case COMMAND_IDX_SCAN:          // 0x01
            cmd_ble_scan((args + 1), arglen, p_err, p_confirm);
            break;

        case COMMAND_IDX_GET_SCANNED:   // 0x02
            cmd_ble_get_scanned((args + 1), arglen, p_err, p_confirm);
            break;

        case COMMAND_IDX_GET_DEVICE:    // 0x03
            cmd_ble_get_device((args + 1), arglen, p_err, p_confirm);
            break;

        case COMMAND_IDX_CONNECT:       // 0x10
            cmd_ble_connect((args + 1), arglen, p_err, p_confirm);
            break;

        default:
            /* Invalid index byte */
            *p_err = ERR_COMMAND_USER;
            break;
    }

    return 0;
}

uint32_t Command_Parse_CHAR(const uint8_t* args, const uint16_t arglen, uint8_t* const p_err, bool* const p_confirm)
{
    switch( *args ) {
        case COMMAND_IDX_DISCOVER:  // 0x01
            cmd_char_discover((args + 1), arglen, p_err, p_confirm);
            break;

        case COMMAND_IDX_GET_DISC:  // 0x02
            cmd_char_get((args + 1), arglen, p_err, p_confirm);
            break;

        case COMMAND_IDX_READ:      // 0x03
            cmd_char_read((args + 1), arglen, p_err, p_confirm);
            break;

        case COMMAND_IDX_WRITE:     // 0x04
            cmd_char_write((args + 1), arglen, p_err, p_confirm);
            break;

        case COMMAND_IDX_SUBSCRIBE: // 0x05
            cmd_char_subscribe((args + 1), arglen, p_err, p_confirm);
            break;

        default:
            /* Invalid index byte */
            *p_err = ERR_COMMAND_USER;
            break;
    }

    return 0;
}

uint32_t Command_Parse_DEVICE(const uint8_t* args, const uint16_t arglen, uint8_t* const p_err, bool* const p_confirm)
{
    switch( *args ) {
        case COMMAND_IDX_STATUS:        // 0x01
            cmd_dev_status((args + 1), arglen, p_err, p_confirm);
            break;

        default:
            /* Invalid index byte */
            *p_err = ERR_COMMAND_USER;
            break;
    }

    return 0;
}

uint32_t Command_Parse_SERV(const uint8_t* args, const uint16_t arglen, uint8_t* const p_err, bool* const p_confirm)
{
    switch( *args ) {
        case COMMAND_IDX_DISCOVER:      // 0x01
            cmd_serv_discover((args + 1), arglen, p_err, p_confirm);
            break;

        case COMMAND_IDX_GET_DISC:      // 0x02
            cmd_serv_get((args + 1), arglen, p_err, p_confirm);
            break;

        default:
            /* Invalid index byte */
            *p_err = ERR_COMMAND_USER;
            break;
    }

    return 0;
}

/* Private functions ---------------------------------------------------------*/

static SERIAL_CMD_DEF(ble_connect)
{
    if( arglen == 1 ) {
        /* received an index from the scan list */
        if( !( Device.GetNumberOfDevices() > *args ) ) {
            *p_err = ERR_INVALID_PARAM;
            return;
        }

        uint8_t* p_args = (uint8_t*) malloc(1);
        *p_args = *args;

        *p_confirm = false;
        *p_err = Device.Schedule(OP_BLE_CONNECT_DEVICE, p_args, 1);
    }
    else if( arglen == BLE_GAP_ADDR_LEN ) {
        /* received an address to connect to */

        uint8_t* p_args = (uint8_t*) malloc(BLE_GAP_ADDR_LEN);
        memcpy(p_args, args, BLE_GAP_ADDR_LEN);

        *p_confirm = false;
        *p_err = Device.Schedule(OP_BLE_CONNECT_DEVICE, p_args, BLE_GAP_ADDR_LEN);
    }
    else {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
    } 
}

static SERIAL_CMD_DEF(ble_get_device)
{
    if( arglen != 1 ) {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
        return;
    }

    if( !( Device.GetNumberOfDevices() > *args ) ) {
        *p_err = ERR_INVALID_PARAM;
        return;
    }

    uint8_t* p_args = (uint8_t*) malloc(1);
    *p_args = *args;

    *p_confirm = false;
    *p_err = Device.Schedule(OP_BLE_GET_DEVICE, p_args, 1);
}

static SERIAL_CMD_DEF(ble_get_scanned)
{
    if( arglen != 0 ) {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
        return;
    }

    *p_confirm = false;
    *p_err = Device.Schedule(OP_BLE_GET_SCANNED, NULL, 0);
}

static SERIAL_CMD_DEF(ble_scan)
{
    if( arglen != 0 ) {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
        return;
    }
    
    *p_confirm = false;
    *p_err = Device.Schedule(OP_BLE_SCAN, NULL, 0);
}

static SERIAL_CMD_DEF(char_discover)
{
    if( arglen != 2 ) {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
        return;
    }

    uint16_t* p_serv = (uint16_t*) malloc(2);
    memcpy((uint8_t*) p_serv, args, 2);

    *p_confirm = false;
    *p_err = Device.Schedule(OP_CHAR_DISCOVER, p_serv, 2);
}

static SERIAL_CMD_DEF(char_get)
{
    if( arglen != 2 ) {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
        return;
    }

    uint16_t* p_serv = (uint16_t*) malloc(2);
    memcpy((uint8_t*) p_serv, args, 2);

    *p_confirm = false;
    *p_err = Device.Schedule(OP_CHAR_GET, p_serv, 2);
}

static SERIAL_CMD_DEF(char_read)
{
    if( arglen != 4 ) {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
        return;
    }

    uint16_t* p_char = (uint16_t*) malloc(4);
    memcpy((uint8_t*) p_char, args, 4);

    *p_confirm = false;
    *p_err = Device.Schedule(OP_CHAR_READ, p_char, 4);
}

static SERIAL_CMD_DEF(char_subscribe)
{
    if( arglen != 5 ) {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
        return;
    }

    uint8_t* p_args = (uint8_t*) malloc(arglen);
    memcpy(p_args, args, arglen);

    *p_confirm = false;
    *p_err = Device.Schedule(OP_CHAR_SUBSCRIBE, p_args, arglen);
}

static SERIAL_CMD_DEF(char_write)
{
    if( arglen < 4 ) {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
        return;
    }

    uint8_t* p_args = (uint8_t*) malloc(arglen);
    memcpy(p_args, args, arglen);

    *p_confirm = false;
    *p_err = Device.Schedule(OP_CHAR_WRITE, p_args, arglen);
}

static SERIAL_CMD_DEF(dev_status)
{
    if( arglen != 0 ) {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
        return;
    }

    *p_confirm = false;
    WRITE_RESPONSE("OK+%02X", Device.GetStatus());
}

static SERIAL_CMD_DEF(serv_discover)
{
    if( arglen != 0 ) {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
        return;
    }

    *p_confirm = false;
    *p_err = Device.Schedule(OP_SERV_DISCOVER, NULL, 0);
}

static SERIAL_CMD_DEF(serv_get)
{
    if( arglen != 0 ) {
        /* bad command */
        *p_err = ERR_COMMAND_USER;
        return;
    }

    *p_confirm = false;
    *p_err = Device.Schedule(OP_SERV_GET, NULL, 0);
}
