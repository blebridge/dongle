/**
 * @file device.c
 * @brief      BLE Bridge device implementation.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <ble_gap.h>
#include <nrf_drv_gpiote.h>
#include <nrf_pwr_mgmt.h>

#include "bridge_ble.h"
#include "device.h"
#include "macros.h"
#include "serial.h"

/**
 * @addtogroup   BLEBridge_Device
 * @{
 */

/**
 * @defgroup   BLEBridge_Device_ic Device Module internal components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

#define BIT_ISSET( reg, bit)    (*reg & (1 << bit))
#define BIT_SET( reg, bit )     (*reg |= (1 << bit))
#define BIT_CLEAR( reg, bit )   (*reg &= ~(1 << bit))

#define ESC_16bit               "%04X"
#define ESC_32bit               "%08lX"

#define LED_SCAN            13

#define QUEUE_LENGTH        5

#define CAST_TO( type, location, offset )   *(((type*) (location)) + offset)

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

typedef struct {
    DeviceOperationType_t   type;
    void*                   args;
    uint16_t                arglen;
} DeviceTask_t;

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief      Clears the busy flag in the device register.
 *
 * @todo       add return value parameter.
 */
static void ble_complete(void);

/**
 * @brief      Sets the connected flag in the device register.
 */
static void ble_connected(void);

/**
 * @brief      Connects to a device, either by index or by BLE address.
 *
 * @param      args    Pointer to either the device index or the BLE address.
 * @param[in]  arglen  The length of @p args.
 *
 * @return     any error that comes up.
 */
static uint32_t ble_connect_device(const uint8_t* args, const uint16_t arglen);

/**
 * @brief      Clears the connected flag in the device register.
 */
static void ble_disconnected(void);

/**
 * @brief      Returns the device info to the user.
 *
 * @param[in]  index  The index of the device in the device list.
 *
 * @return     any error that comes up.
 */
static uint32_t ble_get_deviceinfo(const uint8_t index);

/**
 * @brief      Writes the number of scanned devices to the interface.
 *
 * @return     any error that comes up.
 */
static uint32_t ble_get_scanned(void);

/**
 * @brief      Starts the scanning procedure.
 *
 * @return     any error that comes up.
 */
static uint32_t ble_scan(void);

/**
 * @brief      Callback that is called, when a service discovery is complete.
 *
 * @param[in]  handle  The end handle of the discovery process.
 *
 * @todo       rename function.
 */
static void ble_scancomplete(const uint16_t handle);

/**
 * @brief      Starts a characteristic's discovery under the given service UUID.
 *
 * @param[in]  serv_uuid  The service UUID to discover under.
 *
 * @return     any error that comes up.
 */
static uint32_t char_discover(const uint16_t serv_uuid);

/**
 * @brief      Writes all characteristics discovered undere a given service UUID
 *             to the interface.
 *
 * @param[in]  serv_uuid  The service UUID.
 *
 * @return     any error that comes up.
 */
static uint32_t char_get(const uint16_t serv_uuid);

/**
 * @brief      Writes a notification to the interface.
 *
 * @param[in]  params  Pointer to a 4 byte array, containing the service and
 *                     characteristic UUIDs associated with the notification.
 * @param[in]  p_args  Pointer to the data associated with the notification.
 * @param[in]  arglen  The number of bytes of @p p_args.
 *
 * @return     any error that comes up.
 */
static uint32_t char_notification(const uint16_t* params, const uint8_t* p_args, const uint16_t arglen);

/**
 * @brief      Reads a characteristic value.
 *
 * @param[in]  serv_uuid  The service UUID of the characteristic value.
 * @param[in]  char_uuid  The characteristic UUID.
 *
 * @return     any error that comes up.
 */
static uint32_t char_read(const uint16_t serv_uuid, const uint16_t char_uuid);

/**
 * @brief      Activates notifications of a given characteristic value.
 *
 * @param[in]  serv_uuid  The service UUID of the characteristic value.
 * @param[in]  char_uuid  The characteristic UUID.
 * @param[in]  subs       Flag indicating if notifications (0x01), indications
 *                        (0x02) or none (0x00) should be activated.
 *
 * @return     any error that comes up.
 */
static uint32_t char_subscribe(const uint16_t serv_uuid, const uint16_t char_uuid, const uint8_t subs);

/**
 * @brief      Writes data to a characteristic value.
 *
 * @param[in]  serv_uuid  The service UUID of the characteristic value.
 * @param[in]  char_uuid  The characteristic UUID.
 * @param[in]  p_data     Pointer to the data to be written.
 * @param[in]  size       The number of bytes to write.
 *
 * @return     any error that comes up.
 */
static uint32_t char_write(const uint16_t serv_uuid, const uint16_t char_uuid, const uint8_t* p_data, const uint16_t size);

/**
 * @brief      Gets the status of the device.
 *
 * @return     the status.
 */
static uint8_t get_status(void);

/**
 * @brief      Schedules an operation in the operation queue.
 *
 * @param[in]  op      The operation to be scheduled.
 * @param[in]  args    Pointer to the arguments of the operation.
 * @param[in]  arglen  The number of bytes the arguments hold.
 *
 * @return     any error that comes up.
 *
 * @note       The memory region @p args point to needs to remain valid after
 *             the function returns, i.e. it must be dynamically allocated and
 *             not freed! The memory will be freed after processing the
 *             operation.
 */
static uint8_t schedule(const DeviceOperationType_t op, void* args, const uint16_t arglen);

/**
 * @brief      Discovers the services of the connected device.
 *
 * @return     any error that comes up.
 */
static uint32_t serv_discover(void);

/**
 * @brief      Writes the discovered services to the interface.
 *
 * @return     any error that comes up.
 */
static uint32_t serv_get(void);

/* Private variables ---------------------------------------------------------*/

/*! Internal device register. */
static uint8_t volatile device_register = 0;
/*! Internal operation queue for scheduled operations. */
static DeviceTask_t volatile queue[QUEUE_LENGTH];
/*! Operation queue read pointer */
static volatile DeviceTask_t* volatile p_queue_read;
/*! Operation queue write pointer */
static volatile DeviceTask_t* volatile p_queue_write;
/*! Variable to track progess when scanning for services. */
static volatile uint16_t res_handle = -1;
/*! Access handler to the device functions */
DeviceHandler_t Device;

/**
 * @}
 */

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

uint32_t Device_Init(void)
{
    Device.BLE_Complete = ble_complete;
    Device.BLE_ScanComplete = ble_scancomplete;
    Device.Connected = ble_connected;
    Device.Disconnected = ble_disconnected;
    Device.GetNumberOfDevices = BLE_GetNumberOfScannedDevices;
    Device.GetStatus = get_status;
    Device.Schedule = schedule;

    p_queue_read = queue;
    p_queue_write = queue;

    /** Initialise LEDs */
    nrf_drv_gpiote_out_config_t pin_cfg = GPIOTE_CONFIG_OUT_SIMPLE(true);
    RET_ON_ERROR (nrf_drv_gpiote_out_init(LED_SCAN, &pin_cfg) );

    return 0;
}

uint32_t Device_Process(bool* const pending)
{
    if( p_queue_read->type == OP_NONE ) {
        return 0;
    }

    uint32_t err_code = 0;

    switch( p_queue_read->type ) {
        case OP_BLE_SCAN:
            err_code = ble_scan();
            break;

        case OP_BLE_GET_SCANNED:
            err_code = ble_get_scanned();
            break;

        case OP_BLE_GET_DEVICE:
            err_code = ble_get_deviceinfo( CAST_TO(uint8_t, p_queue_read->args, 0) );
            break;

        case OP_BLE_CONNECT_DEVICE:
            err_code = ble_connect_device( p_queue_read->args, p_queue_read->arglen );
            break;

        case OP_SERV_DISCOVER:
            err_code = serv_discover();
            break;

        case OP_SERV_GET:
            err_code = serv_get();
            break;

        case OP_CHAR_DISCOVER:
            err_code = char_discover( CAST_TO(uint16_t, p_queue_read->args, 0) );
            break;

        case OP_CHAR_GET:
            err_code = char_get( CAST_TO(uint16_t, p_queue_read->args, 0) );
            break;

        case OP_CHAR_READ:
            err_code = char_read( CAST_TO(uint16_t, p_queue_read->args, 0), CAST_TO(uint16_t, p_queue_read->args, 1) );
            break;

        case OP_CHAR_WRITE:
            err_code = char_write(
                CAST_TO(uint16_t, p_queue_read->args, 0),
                CAST_TO(uint16_t, p_queue_read->args, 1),
                (p_queue_read->args + 4),
                p_queue_read->arglen - 4
            );
            break;

        case OP_CHAR_SUBSCRIBE:
            err_code = char_subscribe(
                CAST_TO(uint16_t, p_queue_read->args, 0),
                CAST_TO(uint16_t, p_queue_read->args, 1),
                CAST_TO(uint8_t, p_queue_read->args + 4, 0)
            );
            break;

        case OP_CHAR_NOTIFICATION:
            err_code = char_notification((uint16_t*) p_queue_read->args, (p_queue_read->args + 4), p_queue_read->arglen - 4);
            break;

        default:
            // No implementation needed.
            break;
    }

    free(p_queue_read->args);
    p_queue_read->args = 0;
    p_queue_read->arglen = 0;

    RET_ON_ERROR( err_code );

    /* advance pointer */
    p_queue_read->type = OP_NONE;
    p_queue_read++;
    if( p_queue_read == &queue[QUEUE_LENGTH] ) {
        p_queue_read = queue;
    }

    *pending = (*pending | ( p_queue_read->type != OP_NONE ));
    return err_code;
}

void Device_Ready(void)
{
    WRITE_OK();
}

/* Private functions ---------------------------------------------------------*/

static void ble_complete(void)
{
    BIT_CLEAR(&device_register, DEV_STATUS_BLEBUSY);
    nrf_drv_gpiote_out_clear(LED_SCAN);
}

static void ble_connected(void)
{
    BIT_SET(&device_register, DEV_STATUS_CONNECTED);
    ble_complete();
}

static uint32_t ble_connect_device(const uint8_t* args, const uint16_t arglen)
{
    if( arglen == 1 ) {
        /* Scan index */

        if( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) ) {
            WRITE_ERROR(ERR_DEVICE_BUSY);
            return 0;
        }

        if( BIT_ISSET(&device_register, DEV_STATUS_CONNECTED) ) {
            WRITE_ERROR(ERR_UNSUPPORTED);
            return 0;
        }

        BIT_SET(&device_register, DEV_STATUS_BLEBUSY);
        RET_ON_ERROR( BLE_Connect_Index(*args) );

        WRITE_RESPONSE("OK+%02X", device_register);
    }
    else if( arglen == BLE_GAP_ADDR_LEN ) {
        /* Received address */

        if( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) ) {
            WRITE_ERROR(ERR_DEVICE_BUSY);
            return 0;
        }

        if( BIT_ISSET(&device_register, DEV_STATUS_CONNECTED) ) {
            WRITE_ERROR(ERR_UNSUPPORTED);
            return 0;
        }

        BIT_SET(&device_register, DEV_STATUS_BLEBUSY);
        RET_ON_ERROR( BLE_Connect_Address(args) );

        WRITE_RESPONSE("OK+%02X", device_register);
    }
    else {
        /* Invalid function call, most likely a firmware error. */
        return -1;
    }

    return 0;
}

static void ble_disconnected(void)
{
    BIT_CLEAR(&device_register, DEV_STATUS_CONNECTED);
    ble_complete();
}

static uint32_t ble_get_deviceinfo(const uint8_t index)
{
    const ScanData_t* sdata = BLE_GetScannedDevice(index);

    if( !sdata ) {
        WRITE_ERROR(ERR_INVALID_PARAM);
        return 0;
    }

    /* Write length of response */
    uint16_t len = BLE_GAP_ADDR_LEN + sdata->adv_data.data.len + sdata->sr_data.data.len;
    WRITE_RESPONSE("OK+%04X", len);

    /* Write the data */
    uint8_t* p_buffer = (uint8_t*) malloc(len * 2 + 1);
    uint32_t w_counter = 0;

    Serial_Encode(sdata->peer_addr.pAddr, BLE_GAP_ADDR_LEN, p_buffer, &w_counter);
    Serial_Encode(sdata->adv_data.data.p_data, sdata->adv_data.data.len, p_buffer, &w_counter);
    Serial_Encode(sdata->sr_data.data.p_data, sdata->sr_data.data.len, p_buffer, &w_counter);

    RET_ON_ERROR( Serial_Write(p_buffer, w_counter) );
    Serial_Flush();

    free(p_buffer);

    return 0;
}

static uint32_t ble_get_scanned(void)
{
    WRITE_RESPONSE("OK+%02X", BLE_GetNumberOfScannedDevices() );
    return 0;
}

static uint32_t ble_scan(void)
{
    if( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) ) {
        WRITE_ERROR(ERR_DEVICE_BUSY);
        return 0;
    }

    // ble_busy = true;
    BIT_SET(&device_register, DEV_STATUS_BLEBUSY);
    nrf_drv_gpiote_out_clear(LED_SCAN);

    RET_ON_ERROR( BLE_Scan() );

    WRITE_OK();

    return 0;
}

static void ble_scancomplete(const uint16_t handle)
{
    BIT_CLEAR(&device_register, DEV_STATUS_SCAN);
    res_handle = handle;
}

static uint32_t char_discover(const uint16_t serv_uuid)
{
    if( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) ) {
        WRITE_ERROR(ERR_DEVICE_BUSY);
        return 0;
    }

    if( ! BIT_ISSET(&device_register, DEV_STATUS_CONNECTED) ) {
        WRITE_ERROR(ERR_DEVICE_NOT_CONNECTED);
        return 0;
    }

    BIT_SET(&device_register, DEV_STATUS_BLEBUSY);
    uint32_t err_code = BLE_CHAR_Discover(serv_uuid);

    if( err_code == DEV_ERROR_BLE_PARAM ) {
        WRITE_ERROR(ERR_INVALID_PARAM);
        ble_complete();
        return 0;
    }

    RET_ON_ERROR( err_code );

    /* Wait until the request is complete */
    while( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) ) {
        nrf_pwr_mgmt_run();
    }

    uint16_t n_chars = BLE_GetNumberOfCharacteristics(serv_uuid);
    uint16_t* chars = (uint16_t*) malloc(2*n_chars);
    BLE_GetChars(serv_uuid, chars, n_chars);

    for(uint16_t c = 0; c < n_chars; ++c) {
        BIT_SET(&device_register, DEV_STATUS_BLEBUSY);
        RET_ON_ERROR( BLE_DESC_Discover(chars[c]) );

        while( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) ) {
            nrf_pwr_mgmt_run();
        }
    }

    free(chars);

    WRITE_OK();

    return 0;
}

static uint32_t char_get(const uint16_t serv_uuid)
{
    /* Write length of the response */
    uint16_t n_chars = BLE_GetNumberOfCharacteristics(serv_uuid);
    uint32_t datalen = 2 * n_chars;  // each characteristics identifier is 16 bit.
    WRITE_RESPONSE("OK+" ESC_32bit, datalen );

    /* Write the data */
    uint16_t* chars = (uint16_t*) malloc(datalen);
    if( !chars )    return NRF_ERROR_NO_MEM;
    BLE_GetChars(serv_uuid, chars, n_chars);

    uint8_t* p_buffer = (uint8_t*) malloc(2*datalen + 1);
    if( !p_buffer )    return NRF_ERROR_NO_MEM;
    uint32_t w_counter = 0;

    Serial_Encode((uint8_t*) chars, datalen, p_buffer, &w_counter);
    RET_ON_ERROR( Serial_Write(p_buffer, w_counter) );
    Serial_Flush();

    free(chars);
    free(p_buffer);
    return 0;
}

static uint32_t char_notification(const uint16_t* params, const uint8_t* p_args, const uint16_t arglen)
{
    uint8_t* p_buffer = (uint8_t*) malloc(8 + 2*arglen + 1);
    if( !p_buffer )     return NRF_ERROR_NO_MEM;

    uint32_t w_counter = 0;
    Serial_Encode((uint8_t*) params, 4, p_buffer, &w_counter);
    Serial_Encode(p_args, arglen, p_buffer, &w_counter);

    WRITE_NOTIFICATION("%s", p_buffer);

    free(p_buffer);
    return 0;
}

static uint32_t char_read(const uint16_t serv_uuid, const uint16_t char_uuid)
{
    /* Check if any flags are not as they are supposed to be */
    if( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) ) {
        WRITE_ERROR(ERR_DEVICE_BUSY);
        return 0;
    }

    if( ! BIT_ISSET(&device_register, DEV_STATUS_CONNECTED) ) {
        WRITE_ERROR(ERR_DEVICE_NOT_CONNECTED);
        return 0;
    }

    BIT_SET(&device_register, DEV_STATUS_BLEBUSY);

    /* Issue the read request */
    uint8_t* p_data = 0;
    uint16_t size = 0;
    uint32_t err_code = BLE_CHAR_Read(serv_uuid, char_uuid, &p_data, &size);

    /* Check errors */
    if( err_code == DEV_ERROR_BLE_PARAM ) {
        free(p_data);
        WRITE_ERROR(ERR_INVALID_PARAM);
        ble_complete();
        return 0;
    }
    if( err_code ) {
        free(p_data);
        return err_code;
    }

    /* Wait until the request is complete */
    while( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) ) {
        nrf_pwr_mgmt_run();
    }

    /* Write length of the response */
    WRITE_RESPONSE("OK+" ESC_16bit, size);

    /* Write the data */
    uint8_t* p_buffer = (uint8_t*) malloc(2*size + 1);
    uint32_t w_counter = 0;
    Serial_Encode(p_data, size, p_buffer, &w_counter);

    RET_ON_ERROR( Serial_Write(p_buffer, w_counter) );
    Serial_Flush();

    free(p_data);
    free(p_buffer);

    return 0;
}

static uint32_t char_subscribe(const uint16_t serv_uuid, const uint16_t char_uuid, const uint8_t subs)
{
    /* Check if any flags are not as they are supposed to be */
    if( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) ) {
        WRITE_ERROR(ERR_DEVICE_BUSY);
        return 0;
    }

    if( ! BIT_ISSET(&device_register, DEV_STATUS_CONNECTED) ) {
        WRITE_ERROR(ERR_DEVICE_NOT_CONNECTED);
        return 0;
    }

    BIT_SET(&device_register, DEV_STATUS_BLEBUSY);

    RET_ON_ERROR( BLE_CHAR_Subscribe(serv_uuid, char_uuid, subs) );

    /* Wait until the request is complete */
    while( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) )    nrf_pwr_mgmt_run();

    WRITE_OK();

    return 0;
}

static uint32_t char_write(const uint16_t serv_uuid, const uint16_t char_uuid, const uint8_t* p_data, const uint16_t size)
{
    /* Check if any flags are not as they are supposed to be */
    if( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) ) {
        WRITE_ERROR(ERR_DEVICE_BUSY);
        return 0;
    }

    if( ! BIT_ISSET(&device_register, DEV_STATUS_CONNECTED) ) {
        WRITE_ERROR(ERR_DEVICE_NOT_CONNECTED);
        return 0;
    }

    uint16_t n_bytes = size;

    if( n_bytes > 224 ) {

        do {
            uint16_t bytes_to_write = (n_bytes > 224)? 224 : n_bytes;
            BIT_SET(&device_register, DEV_STATUS_BLEBUSY);

            uint16_t offs = size-n_bytes;
            // uint32_t err = BLE_CHAR_LongWritePrepare(serv_uuid, char_uuid, &p_data[offs], bytes_to_write, offs);
            RET_ON_ERROR( BLE_CHAR_Write(serv_uuid, char_uuid, &p_data[offs], bytes_to_write) );

            // RET_ON_ERROR( BLE_CHAR_Write(serv_uuid, char_uuid, p_data, bytes_to_write, size-n_bytes) );

            // printf("!00000000 Prepare: %u, %u (%lu)%c\r\n", bytes_to_write, offs, err, 0xFA);
            // RET_ON_ERROR(err);

            while( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) )    nrf_pwr_mgmt_run();

            n_bytes -= bytes_to_write;

        } while(n_bytes);

    }
    else {
        BIT_SET(&device_register, DEV_STATUS_BLEBUSY);
        RET_ON_ERROR( BLE_CHAR_Write(serv_uuid, char_uuid, p_data, n_bytes) );
        while( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) )    nrf_pwr_mgmt_run();
    }



    // RET_ON_ERROR( BLE_CHAR_Write(serv_uuid, char_uuid, p_data, size) );

    /* Wait until the request is complete */
    // while( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) )    nrf_pwr_mgmt_run();

    WRITE_OK();

    return 0;
}

static uint8_t get_status(void)
{
    return device_register;
}

static uint8_t schedule(const DeviceOperationType_t op, void* args, const uint16_t arglen)
{
    if( p_queue_write->type != OP_NONE ) {
        return 1;
    }

    p_queue_write->type = op;
    p_queue_write->args = args;
    p_queue_write->arglen = arglen;

    p_queue_write++;
    if( p_queue_write == &queue[QUEUE_LENGTH] ) {
        p_queue_write = queue;
    }

    return 0;
}

static uint32_t serv_discover(void)
{
    if( BIT_ISSET(&device_register, DEV_STATUS_BLEBUSY) ) {
        WRITE_ERROR(ERR_DEVICE_BUSY);
        return 0;
    }

    if( ! BIT_ISSET(&device_register, DEV_STATUS_CONNECTED) ) {
        WRITE_ERROR(ERR_DEVICE_NOT_CONNECTED);
        return 0;
    }

    BIT_SET(&device_register, DEV_STATUS_BLEBUSY);
    
    uint16_t start_handle = 0x0001;
    res_handle = -1;
    
    while(1) {

        BIT_SET(&device_register, DEV_STATUS_SCAN);
        RET_ON_ERROR( BLE_SERV_Discover(start_handle) );

        /* Wait until the request is complete */
        while( BIT_ISSET(&device_register, DEV_STATUS_SCAN) )    nrf_pwr_mgmt_run();

        if( res_handle == ((uint16_t) -1) || res_handle < start_handle ) {
            break;
        }

        start_handle = res_handle + 1;
    }

    BIT_CLEAR(&device_register, DEV_STATUS_BLEBUSY);
    

    WRITE_OK();

    return 0;
}

static uint32_t serv_get(void)
{
    /* Write length of the response */
    uint16_t n_services = BLE_GetNumberOfServices();
    uint32_t datalen = 2 * n_services;  // each service identifier is 16 bit.
    WRITE_RESPONSE("OK+" ESC_32bit, datalen );

    /* Write the data */
    uint16_t* services = (uint16_t*) malloc(n_services);
    if( !services ) return NRF_ERROR_NO_MEM;
    BLE_GetServices(services, n_services);

    uint8_t* p_buffer = (uint8_t*) malloc(2*datalen + 1);
    uint32_t w_counter = 0;

    Serial_Encode((uint8_t*) services, datalen, p_buffer, &w_counter);
    RET_ON_ERROR( Serial_Write(p_buffer, w_counter) );

    // RET_ON_ERROR( Serial_Write((uint8_t*) services, datalen) );
    Serial_Flush();

    free(services);
    free(p_buffer);

    return 0;
}
