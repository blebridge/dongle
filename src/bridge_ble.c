/**
 * @file bridge_ble.c
 * @brief      BLE Bridge stack implementation.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdlib.h>
#include <app_error.h>
#include <ble_conn_state.h>
#include <nrf_ble_gatt.h>
#include <nrf_ble_scan.h>
#include <nrf_fstorage.h>
#include <nrf_log.h>
#include <nrf_pwr_mgmt.h>
#include <nrf_sdh.h>
#include <nrf_sdh_ble.h>
#include <nrf_sdh_soc.h>
#include <peer_manager_handler.h>
#include "device.h"
#include "macros.h"
#include "bridge_ble.h"

#include <nrf_delay.h>

/**
 * @addtogroup   BLEBridge_Stack
 * @{
 */

/**
 * @defgroup   BLEBridge_Stack_ic BLE stack internal components
 * @{
 */

/* Private defines -----------------------------------------------------------*/

/*! Maximum number of devices in the scan list. */
#define SCAN_MAX_DEVICES                32

/*! Duration of the scanning in units of 10 milliseconds. */
#define SCAN_DURATION_WITELIST          1000

/*! A tag identifying the SoftDevice BLE configuration. */
#define APP_BLE_CONN_CFG_TAG            1

/*! Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_OBSERVER_PRIO           3
/*! Applications' SoC observer priority. You shouldn't need to modify this value. */
#define APP_SOC_OBSERVER_PRIO           1

/*! Perform bonding. */
#define SEC_PARAM_BOND                  1
/*! Man In The Middle protection not required. */
#define SEC_PARAM_MITM                  0
/*! LE Secure Connections not enabled. */
#define SEC_PARAM_LESC                  0
/*! Keypress notifications not enabled. */
#define SEC_PARAM_KEYPRESS              0
/*! No I/O capabilities. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE
/*! Out Of Band data not available. */
#define SEC_PARAM_OOB                   0
/*! Minimum encryption key size. */
#define SEC_PARAM_MIN_KEY_SIZE          7
/*! Maximum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16

#define TARGET_UUID               BLE_UUID_GATT                    /**< Target device name that application is looking for. */

/* Private macros ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/**
 * @brief      Helper struct to manage the characteristics of a device.
 */
typedef struct BLE_Char_t {
    ble_gattc_char_t        gattc_char;     /*!< The GATTC parameters of the characteristic. */
    uint16_t                cccd_handle;    /*!< If available, the handle to the CCCD. */
    struct BLE_Char_t*      next;           /*!< Pointer to the next object in the linked list. */
} BLE_Char_t;

/**
 * @brief      Helper struct to manage the services of a device.
 */
typedef struct BLE_Service_t {
    ble_gattc_service_t     gattc_service;  /*!< The GATTC parameters of the service. */
    BLE_Char_t*             characteristic; /*!< Pointer to the first characteristic in the linked list. */
    uint16_t                char_count;     /*!< The number of characteristics discovered. */
    struct BLE_Service_t*   next;           /*!< Pointer to the next object in the linked list. */
} BLE_Service_t;

/**
 * @brief      Helper struct to manage the connection.
 */
typedef struct {
    uint16_t                conn_handle;    /*!< The connection handle. */
    BLE_Service_t*          service;        /*!< Pointer to the first service in the linked list. */
    uint16_t                service_count;  /*!< The number of services discovered. */
    
    BLE_Service_t*  p_char_desc_service;    /*!< Pointer to the service whose characteristics are currently
                                             * being discovered. */
    BLE_Char_t*     p_char_desc_char;       /*!< Pointer to the characteristics whose descriptors are currently
                                             * being discovered. */
    uint16_t        disc_desc_end_handle;   /*!< The end handle of the description discovery procedure. */
    uint8_t**       p_read_buffer;          /*!< Pointer to store the memory location of an allocated memory
                                             * region for storing read data. */
    uint16_t*       p_read_size;            /*!< Pointer to the variable holding the size of the data read. */
} ConnectionManager_t;

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief      Callback function on an advertisement report event.
 *
 * @param[in]  p_report  Pointer to the event data.
 */
static void cb_adv_report(const ble_gap_evt_adv_report_t* const p_report);

/**
 * @brief      Callback function on a BLE event.
 *
 * @param[in]  p_ble_evt  Pointer to the event data.
 * @param[in]  p_context  Pointer to the context of the event.
 */
static void cb_ble_event(ble_evt_t const * p_ble_evt, void * p_context);

/**
 * @brief      Callback function on a peer manager event.
 *
 * @param[in]  p_evt  Pointer to the event data.
 */
static void cb_pm_event(pm_evt_t const * p_evt);

/**
 * @brief      Callback function on a scan event.
 *
 * @param[in]  p_scan_evt  Pointer to the event data.
 */
static void cb_scan_event(scan_evt_t const * p_scan_evt);

/**
 * @brief      Callback function on an SOC event.
 *
 * @param[in]  evt_id     The event identifier.
 * @param[in]  p_context  The context of the event.
 */
static void cb_soc_event(uint32_t evt_id, void * p_context);

/**
 * @brief      Clears the linked list of characteristics of a given service
 *             object.
 *
 * @param      p_service  Pointer to the service object.
 */
static void clear_characteristics(BLE_Service_t* const p_service);

/**
 * @brief      Clears the linked list of services of the connection.
 */
static void clear_services(void);

/**
 * @brief      Initialises the GATT module.
 *
 * @return     any error that comes up.
 */
static uint32_t gatt_init(void);

/**
 * @brief      Gets the characteristic's handle from it's UUID.
 *
 * @param[in]  char_uuid            The UUID of the characteristic.
 * @param[in]  p_service            Pointer to the service of the
 *                                  characteristic.
 * @param[out] pp_characteristic    Pointer to store the address of the
 *                                  characteristic's handle.
 *
 * @retval     0                    if the handle was found and @p
 *                                  pp_characteristic was set.
 * @retval     DEV_ERROR_BLE_PARAM  if the handle could not be found.
 */
static uint32_t get_char_handle_from_UUID(const uint16_t char_uuid, const BLE_Service_t* p_service, BLE_Char_t** const pp_characteristic);

/**
 * @brief      Gets the characteristic's UUID from it's value handle.
 *
 * @param[in]  value_handle  The characteristic's value handle.
 * @param[out] p_serv_uuid   Pointer to store the service UUID.
 * @param[out] p_char_uuid   Pointer to store the characteristic UUID.
 *
 * @return     any error that comes up.
 */
static uint32_t get_char_UUID_from_value_handle(const uint16_t value_handle, uint16_t* const p_serv_uuid, uint16_t* const p_char_uuid);

/**
 * @brief      Gets the service's handle from it's UUID.
 *
 * @param[in]  serv_uuid            The UUID of the service.
 * @param[out] pp_service           Pointer to store the address if the
 *                                  service's handle.
 *
 * @retval     0                    if the handle was found and @p pp_service
 *                                  was set.
 * @retval     DEV_ERROR_BLE_PARAM  if the handle could not be found.
 */
static uint32_t get_service_handle_from_UUID(const uint16_t serv_uuid, BLE_Service_t** const pp_service);

/**
 * @brief      { function_description }
 *
 * @param      p_peers  The peers
 * @param      p_size   The size
 */
// static void peer_list_get(pm_peer_id_t * p_peers, uint32_t * p_size);

/**
 * @brief      Initialises the peer manager module.
 *
 * @return     any error that comes up.
 */
static uint32_t peer_manager_init(void);

/**
 * @brief      Scans an event complete.
 *
 * @param[in]  pReport  The report
 */
// static void scan_event_complete(const ble_gap_evt_adv_report_t* const pReport);

/**
 * @brief      Sets the scan filter.
 *
 * @return     any error that comes up.
 */
static uint32_t scan_filters_set(void);

/**
 * @brief      Initialises the scan module.
 *
 * @return     any error that comes up.
 */
static uint32_t scan_init(void);

/**
 * @brief      Starts scanning.
 *
 * @return     any error that comes up.
 */
static uint32_t scan_start(void);

/**
 * @brief      { function_description }
 */
// static uint32_t whitelist_load(void);

/**
 * @brief      { function_description }
 */
// static void whitelist_request(void);

/* Private variables ---------------------------------------------------------*/

static ConnectionManager_t manager = { .conn_handle = BLE_CONN_HANDLE_INVALID };

static ble_gap_scan_params_t const scan_conn_params = {
        .active        = 0x01,
        .interval      = 160,
        .window        = NRF_BLE_SCAN_SCAN_WINDOW,
        .filter_policy = BLE_GAP_SCAN_FP_ACCEPT_ALL, //BLE_GAP_SCAN_FP_WHITELIST,
        .timeout       = 500,
        .scan_phys     = BLE_GAP_PHY_1MBPS,
    };



// static uint16_t              m_conn_handle;               /**< Current connection handle. */
static bool                  m_memory_access_in_progress; /**< Flag to keep track of ongoing operations on persistent memory. */
// static bool                  m_whitelist_disabled;        /**< True if whitelist has been temporarily disabled. */
NRF_BLE_GATT_DEF(m_gatt);                                 /**< GATT module instance. */
NRF_BLE_SCAN_DEF(m_scan);                                 /**< Scanning Module instance. */

static char const m_target_periph_name[] = "Nordic_GATTS"; /**< If you want to connect to a peripheral using a given advertising name, type its name here. */
static bool       is_connect_per_addr    = false;          /**< If you want to connect to a peripheral with a given address, set this to true and put the correct address in the variable below. */

/**< Scan parameters requested for scanning and connection. */
static ble_gap_scan_params_t const m_scan_param =
{
    .active        = 0x01,
    .interval      = NRF_BLE_SCAN_SCAN_INTERVAL,
    .window        = NRF_BLE_SCAN_SCAN_WINDOW,
    .filter_policy = BLE_GAP_SCAN_FP_ACCEPT_ALL, //BLE_GAP_SCAN_FP_WHITELIST,
    .timeout       = SCAN_DURATION_WITELIST,
    .scan_phys     = BLE_GAP_PHY_1MBPS,
};

static ble_gap_addr_t const m_target_periph_addr =
{
    /* Possible values for addr_type:
       BLE_GAP_ADDR_TYPE_PUBLIC,
       BLE_GAP_ADDR_TYPE_RANDOM_STATIC,
       BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_RESOLVABLE,
       BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_NON_RESOLVABLE. */
      .addr_type = BLE_GAP_ADDR_TYPE_RANDOM_STATIC,
      .addr      = {0x8D, 0xFE, 0x23, 0x86, 0x77, 0xD9}
};

static ScanData_t* scanned_devices[SCAN_MAX_DEVICES] = {0};
static uint8_t n_scanned_devices = 0;
// static ble_gattc_char_t* m_conn_chars = 0;
// static uint16_t m_conn_char_count = 0;

/**
 * @}
 */

/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/

uint32_t BLE_CHAR_Discover(const uint16_t serv_uuid)
{
    if( get_service_handle_from_UUID(serv_uuid, &manager.p_char_desc_service) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    return sd_ble_gattc_characteristics_discover(manager.conn_handle, &(manager.p_char_desc_service->gattc_service.handle_range));
}

uint32_t BLE_CHAR_LongWriteExec(const uint16_t serv_uuid, const uint16_t char_uuid)
{
    BLE_Service_t* p_service = 0;
    BLE_Char_t* p_characteristic = 0;

    if( get_service_handle_from_UUID(serv_uuid, &p_service) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    if( get_char_handle_from_UUID(char_uuid, p_service, &p_characteristic) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    ble_gattc_write_params_t params = {
        // .write_op = BLE_GATT_OP_PREP_WRITE_REQ,
        .write_op = BLE_GATT_OP_EXEC_WRITE_REQ,
        .flags =    BLE_GATT_EXEC_WRITE_FLAG_PREPARED_WRITE,
        .handle =   p_characteristic->gattc_char.handle_value,
    };

    RET_ON_ERROR( sd_ble_gattc_write(manager.conn_handle, &params) );

    return 0;
}

uint32_t BLE_CHAR_LongWritePrepare(const uint16_t serv_uuid, const uint16_t char_uuid, const uint8_t* p_data, const uint16_t size, const uint16_t offs)
{
    BLE_Service_t* p_service = 0;
    BLE_Char_t* p_characteristic = 0;

    if( get_service_handle_from_UUID(serv_uuid, &p_service) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    if( get_char_handle_from_UUID(char_uuid, p_service, &p_characteristic) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    ble_gattc_write_params_t params = {
        // .write_op = BLE_GATT_OP_PREP_WRITE_REQ,
        .write_op = BLE_GATT_OP_PREP_WRITE_REQ,
        // .flags =    BLE_GATT_EXEC_WRITE_FLAG_PREPARED_WRITE,
        .handle =   p_characteristic->gattc_char.handle_value,
        .offset =   offs,
        .len =      size,
        .p_value =  p_data
    };

    RET_ON_ERROR( sd_ble_gattc_write(manager.conn_handle, &params) );

    return 0;
}

uint32_t BLE_CHAR_Read(const uint16_t serv_uuid, const uint16_t char_uuid, uint8_t** p_data, uint16_t* const p_size)
{
    BLE_Service_t* p_service = 0;
    BLE_Char_t* p_characteristic = 0;

    if( get_service_handle_from_UUID(serv_uuid, &p_service) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    if( get_char_handle_from_UUID(char_uuid, p_service, &p_characteristic) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    manager.p_read_buffer = p_data;
    manager.p_read_size = p_size;

    RET_ON_ERROR( sd_ble_gattc_read(manager.conn_handle, p_characteristic->gattc_char.handle_value, 0) );

    return 0;
}

uint32_t BLE_CHAR_Subscribe(const uint16_t serv_uuid, const uint16_t char_uuid, const uint8_t subs)
{
    BLE_Service_t* p_service = 0;
    BLE_Char_t* p_characteristic = 0;

    if( get_service_handle_from_UUID(serv_uuid, &p_service) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    if( get_char_handle_from_UUID(char_uuid, p_service, &p_characteristic) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    if( !p_characteristic->cccd_handle ) {
        return DEV_ERROR_BLE_PARAM;
    }

    /* Subscribe to notifications */
    ble_gattc_write_params_t params = {
        // .write_op = BLE_GATT_OP_PREP_WRITE_REQ,
        .write_op   = BLE_GATT_OP_WRITE_CMD,
        .flags      = BLE_GATT_EXEC_WRITE_FLAG_PREPARED_WRITE,
        .handle     = p_characteristic->cccd_handle,
        .offset     = 0,
        .len        = 1,
        .p_value    = &subs
    };

    RET_ON_ERROR( sd_ble_gattc_write(manager.conn_handle, &params) );

    return 0;
}

uint32_t BLE_CHAR_Write(const uint16_t serv_uuid, const uint16_t char_uuid, const uint8_t* p_data, const uint16_t size)
{
    BLE_Service_t* p_service = 0;
    BLE_Char_t* p_characteristic = 0;

    if( get_service_handle_from_UUID(serv_uuid, &p_service) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    if( get_char_handle_from_UUID(char_uuid, p_service, &p_characteristic) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    ble_gattc_write_params_t params = {
        // .write_op = BLE_GATT_OP_PREP_WRITE_REQ,
        .write_op = BLE_GATT_OP_WRITE_CMD,
        .flags =    BLE_GATT_EXEC_WRITE_FLAG_PREPARED_WRITE,
        .handle =   p_characteristic->gattc_char.handle_value,
        .offset =   0,
        .len =      size,
        .p_value =  p_data
    };

    RET_ON_ERROR( sd_ble_gattc_write(manager.conn_handle, &params) );

    return 0;
}

uint32_t BLE_Connect_Address(const uint8_t* p_addr)
{
    ble_gap_addr_t conn_addr = {0};
    conn_addr.addr_type = BLE_GAP_ADDR_TYPE_RANDOM_STATIC;
    memcpy(conn_addr.addr, p_addr, BLE_GAP_ADDR_LEN);

    RET_ON_ERROR( sd_ble_gap_connect(&conn_addr, &scan_conn_params, &m_scan.conn_params, APP_BLE_CONN_CFG_TAG) );
    return 0;   
}

uint32_t BLE_Connect_Index(const uint8_t index)
{
    if( ! (SCAN_MAX_DEVICES > index) )  return -1;

    const ScanData_t* device = BLE_GetScannedDevice(index);
    if( ! device )  return -1;

    RET_ON_ERROR( sd_ble_gap_connect(&device->peer_addr_new, &scan_conn_params, &m_scan.conn_params, APP_BLE_CONN_CFG_TAG) );

    return 0;
}

uint32_t BLE_DESC_Discover(const uint16_t char_uuid)
{
    if( get_char_handle_from_UUID(char_uuid, manager.p_char_desc_service, &manager.p_char_desc_char) ) {
        return DEV_ERROR_BLE_PARAM;
    }

    manager.disc_desc_end_handle = manager.p_char_desc_char->next ? (manager.p_char_desc_char->next->gattc_char.handle_decl - 1) : manager.p_char_desc_service->gattc_service.handle_range.end_handle;

    ble_gattc_handle_range_t range = {
        .start_handle = manager.p_char_desc_char->gattc_char.handle_decl,
        .end_handle = manager.disc_desc_end_handle
    };

    return sd_ble_gattc_descriptors_discover(manager.conn_handle, &range);
}

void BLE_GetChars(const uint16_t serv_uuid, uint16_t* p_buf, const uint16_t size)
{
    BLE_Service_t* p_service = 0;

    if( get_service_handle_from_UUID(serv_uuid, &p_service) ) {
        return;
    }

    uint16_t n_chars = (size > p_service->char_count) ? p_service->char_count : size;
    const BLE_Char_t* p_characteristic = p_service->characteristic;

    for(uint16_t i = 0; i < n_chars; ++i) {
        p_buf[i] = p_characteristic->gattc_char.uuid.uuid;
        p_characteristic = p_characteristic->next;
    }
}

uint16_t BLE_GetNumberOfCharacteristics(const uint16_t serv_uuid)
{
    BLE_Service_t* p_service = 0;

    if( get_service_handle_from_UUID(serv_uuid, &p_service) ) {
        return 0;
    }

    return p_service->char_count;
}

uint8_t BLE_GetNumberOfScannedDevices(void)
{
    return n_scanned_devices;
}

uint16_t BLE_GetNumberOfServices(void)
{
    return manager.service_count;
}

const ScanData_t* const BLE_GetScannedDevice(const uint8_t index)
{
    if( ! (SCAN_MAX_DEVICES > index) )  return (ScanData_t*) 0;

    return scanned_devices[index];
}

void BLE_GetServices(uint16_t* p_buf, const uint16_t size)
{
    uint16_t n_services = (size > manager.service_count) ? manager.service_count : size;
    const BLE_Service_t* p_service = manager.service;

    for(uint16_t i = 0; i < n_services; ++i) {
        p_buf[i] = p_service->gattc_service.uuid.uuid;
        p_service = p_service->next;
    }
}

uint32_t BLE_Init(void)
{
    memset((uint8_t*) &manager, 0, sizeof(ConnectionManager_t));
    manager.conn_handle = BLE_CONN_HANDLE_INVALID;

    /* Enable power management. */
    RET_ON_ERROR( nrf_pwr_mgmt_init() );

    /* Initialise SD */
    RET_ON_ERROR( nrf_sdh_enable_request() );

    /* Configure the BLE stack using the default settings. Fetch the start
     * address of the application RAM. */
    uint32_t ram_start = 0;
    RET_ON_ERROR( nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start) );

    /* Enable BLE stack */
    RET_ON_ERROR( nrf_sdh_ble_enable(&ram_start) );

    // Register handlers for BLE and SoC events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, cb_ble_event, NULL);
    NRF_SDH_SOC_OBSERVER(m_soc_observer, APP_SOC_OBSERVER_PRIO, cb_soc_event, NULL);

    /* Configure SD */
    RET_ON_ERROR( gatt_init() );
    RET_ON_ERROR( peer_manager_init() );
    RET_ON_ERROR( scan_init() );

    ble_opt_t opts = { 0 };
    opts.gattc_opt.uuid_disc.auto_add_vs_enable = 1;
    RET_ON_ERROR( sd_ble_opt_set(BLE_GATTC_OPT_UUID_DISC, &opts) );

#if 0
    RET_ON_ERROR( gap_params_init() );
    RET_ON_ERROR( services_init(cfg) );
    RET_ON_ERROR( advertising_init() );
    RET_ON_ERROR( conn_params_init() );
    RET_ON_ERROR( peer_manager_init() );
    RET_ON_ERROR( advertising_start(false) );
#endif

    return 0;
}

uint32_t BLE_Scan(void)
{
#if 0
    nrf_ble_scan_init_t init_scan = {0};

    init_scan.p_scan_param     = &m_scan_param;
    // init_scan.connect_if_match = true;
    init_scan.connect_if_match = false;
    init_scan.conn_cfg_tag     = APP_BLE_CONN_CFG_TAG;

    RET_ON_ERROR( nrf_ble_scan_init(&m_scan, &init_scan, cb_scan_event) );
    RET_ON_ERROR( scan_filters_set() );
    RET_ON_ERROR( whitelist_load() );
#endif
    // RET_ON_ERROR( nrf_ble_scan_init(&m_scan, NULL, cb_scan_event) );
    // RET_ON_ERROR( nrf_ble_scan_params_set(&m_scan, &m_scan_param) );

    (void) scan_filters_set;
    // (void) cb_scan_event;

    return scan_start();
}

uint32_t BLE_SERV_Discover(const uint16_t start_handle)
{
    /* If we start from the first handle, clear all services already discovered. */
    if( start_handle == 0x0001 ) {
        clear_services();
    }

    RET_ON_ERROR( sd_ble_gattc_primary_services_discover(manager.conn_handle, start_handle, NULL) );

    return 0;
}

/* Private functions ---------------------------------------------------------*/

static void cb_adv_report(const ble_gap_evt_adv_report_t* const p_report)
{
    uint64_t l_addr;
    ScanData_t* p_device;

    /* Iterate over device list to see if a new device was detected */
    for( uint8_t i = 0; i < SCAN_MAX_DEVICES; ++i ) {

        p_device = scanned_devices[i];
        l_addr = 0;

        /* Parse the long address from the scan event */
        memcpy((uint8_t*) &l_addr, p_report->peer_addr.addr, BLE_GAP_ADDR_LEN);

        /* Add the device if the list was traversed without a match */
        if( p_device == 0 ) {

            /* Allocate memory for the device */
            p_device = (ScanData_t*) malloc(sizeof(ScanData_t));
            if( !p_device )   APP_ERROR_CHECK(NRF_ERROR_NO_MEM);
            memset(p_device, 0, sizeof(ScanData_t));

            /* Copy the peer address */
            p_device->peer_addr_new = p_report->peer_addr;

            p_device->peer_addr.lAddr = l_addr;
            // memcpy(p_device->addr.pAddr, p_report->peer_addr.addr, BLE_GAP_ADDR_LEN);

            /* Copy the report data */
            p_device->adv_data.type.bitfield = p_report->type;
            p_device->adv_data.data.len = p_report->data.len;

            p_device->adv_data.data.p_data = (uint8_t*) malloc(p_device->adv_data.data.len);
            if( !p_device->adv_data.data.p_data )    APP_ERROR_CHECK(NRF_ERROR_NO_MEM);
            memcpy(p_device->adv_data.data.p_data, p_report->data.p_data, p_device->adv_data.data.len);

            scanned_devices[i] = p_device;
            n_scanned_devices++;
            
            break;
        }
        /* We have a match! */
        else if( p_device->peer_addr.lAddr == l_addr ) {

            if( p_report->type.scan_response && !p_device->sr_data_valid ) {
                /* Copy scan response date */
                p_device->sr_data.type.bitfield = p_report->type;
                p_device->sr_data.data.len = p_report->data.len;

                p_device->sr_data.data.p_data = (uint8_t*) malloc(p_device->sr_data.data.len);
                if( !p_device->sr_data.data.p_data ) APP_ERROR_CHECK(NRF_ERROR_NO_MEM);
                memcpy(p_device->sr_data.data.p_data, p_report->data.p_data, p_device->sr_data.data.len);
            }

            break;
        }
    }
}

static void cb_ble_event(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t              err_code;
    ble_gap_evt_t const *   p_gap_evt = &p_ble_evt->evt.gap_evt;
    ble_gattc_evt_t const*  p_gattc_evt = &p_ble_evt->evt.gattc_evt;

    // pm_handler_secure_on_connection(p_ble_evt);

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
        {
            Device.Connected();
            manager.conn_handle = p_gap_evt->conn_handle;

            /* @todo       make the device somehow automatically discover
             *             services after a successful connection. For some
             *             reason neither directly calling the SD nor scheduling
             *             the discovery works... Inserting a delay here works,
             *             but do we want a delay in a callback? */

            // APP_ERROR_CHECK( BLE_SERV_Discover() );
            // APP_ERROR_CHECK( Device.Schedule(OP_SERV_DISCOVER, NULL, 0) );
        } break;

        case BLE_GAP_EVT_DISCONNECTED:
        {
            Device.Disconnected();
            manager.conn_handle = BLE_CONN_HANDLE_INVALID;
        } break;

        case BLE_GATTC_EVT_DESC_DISC_RSP:
        {
            for(uint16_t i = 0; i < p_gattc_evt->params.desc_disc_rsp.count; ++i) {
                /* Check for CCCD */
                if( p_gattc_evt->params.desc_disc_rsp.descs[i].uuid.uuid == 0x2902 ) {
                    manager.p_char_desc_char->cccd_handle = p_gattc_evt->params.desc_disc_rsp.descs[i].handle;
                }
            }

            if( ! p_gattc_evt->gatt_status ) {
                ble_gattc_handle_range_t nrange = {
                    .start_handle = p_gattc_evt->params.desc_disc_rsp.descs[p_gattc_evt->params.desc_disc_rsp.count - 1].handle + 1,
                    .end_handle = manager.disc_desc_end_handle
                };

                APP_ERROR_CHECK( sd_ble_gattc_descriptors_discover(manager.conn_handle, &nrange) );
            }
            else {
                Device.BLE_Complete();
            }
        } break;

        case BLE_GATTC_EVT_PRIM_SRVC_DISC_RSP:
        {
            const ble_gattc_evt_prim_srvc_disc_rsp_t* rsp = &p_ble_evt->evt.gattc_evt.params.prim_srvc_disc_rsp;

            /* Allocate memory to store the discovered services */
            BLE_Service_t** parent = &manager.service;
            while( *parent )    parent = &((*parent)->next);

            manager.service_count += rsp->count;
            uint16_t end_handle = -1;

            for(uint16_t i = 0; i < rsp->count; ++i) {
                *parent = (BLE_Service_t*) malloc(sizeof(BLE_Service_t));
                if( ! *parent ) APP_ERROR_CHECK(NRF_ERROR_NO_MEM);
                memset((uint8_t*) *parent, 0, sizeof(BLE_Service_t));
                (*parent)->gattc_service = rsp->services[i];
                end_handle = (*parent)->gattc_service.handle_range.end_handle;
                parent = &((*parent)->next);
            }

            /* @todo       if the last received service doesn't use 0xFFFF as
             *             end handle, trigger another discovery with this end
             *             handle as start handle. Maybe use Device.Schedule for
             *             that, with a flag skipping the BLE_BUSY check? */

            Device.BLE_ScanComplete(end_handle);

        } break;

        case BLE_GATTC_EVT_CHAR_DISC_RSP:
        {
            const ble_gattc_evt_char_disc_rsp_t* rsp = &p_ble_evt->evt.gattc_evt.params.char_disc_rsp;

            /* Delete existing list */
            clear_characteristics(manager.p_char_desc_service);

            /* Allocate memory to store the discovered characteristics */
            BLE_Char_t** parent = &(manager.p_char_desc_service->characteristic);
            manager.p_char_desc_service->char_count = rsp->count;

            for(uint16_t i = 0; i < rsp->count; ++i) {
                *parent = (BLE_Char_t*) malloc(sizeof(BLE_Char_t));
                if( ! *parent ) APP_ERROR_CHECK(NRF_ERROR_NO_MEM);
                memset((uint8_t*) *parent, 0, sizeof(BLE_Char_t));
                (*parent)->gattc_char = rsp->chars[i];
                parent = &((*parent)->next);
            }

            Device.BLE_Complete();
        } break;

        case BLE_GATTC_EVT_READ_RSP:
        {
            if( manager.p_read_buffer && manager.p_read_size ) {
                *manager.p_read_size = p_ble_evt->evt.gattc_evt.params.read_rsp.len;

                *manager.p_read_buffer = (uint8_t*) malloc(*manager.p_read_size);

                memcpy(*manager.p_read_buffer, p_ble_evt->evt.gattc_evt.params.read_rsp.data, *manager.p_read_size);

                manager.p_read_buffer = 0;
                manager.p_read_size = 0;
            }

            /* @todo       What do we do, if the value is too long? Where should
             *             this be handled? */

            Device.BLE_Complete();
        } break;

        case BLE_GATTC_EVT_WRITE_CMD_TX_COMPLETE:
        {
            Device.BLE_Complete();
        } break;

        case BLE_GATTC_EVT_HVX:
        {
            if( p_gattc_evt->params.hvx.type == BLE_GATT_HVX_NOTIFICATION ) {

                uint16_t arglen = 4 + p_gattc_evt->params.hvx.len;
                uint8_t* args = (uint8_t*) malloc(arglen);
                APP_ERROR_CHECK( args == NULL );

                APP_ERROR_CHECK( get_char_UUID_from_value_handle(p_gattc_evt->params.hvx.handle, (uint16_t*) &args[0] , (uint16_t*) &args[2]) );
                memcpy(&args[4], p_gattc_evt->params.hvx.data, p_gattc_evt->params.hvx.len);
                APP_ERROR_CHECK( Device.Schedule(OP_CHAR_NOTIFICATION, args, arglen) );
            }
            else {
                /* Unsupported */
            }
        } break;


        /*
         * @todo       The following event handlers need to be further tweaked!
         */
        case BLE_GATTC_EVT_CHAR_VAL_BY_UUID_READ_RSP:
        {
            printf("Read CHAR UUID (%u), %u:\r\n", p_ble_evt->evt.gattc_evt.gatt_status, p_ble_evt->evt.gattc_evt.params.char_val_by_uuid_read_rsp.count);

            for(uint16_t i = 0; i < p_ble_evt->evt.gattc_evt.params.char_val_by_uuid_read_rsp.count; ++i) {
                printf("  [%u]  0x%02X\r\n", i, p_ble_evt->evt.gattc_evt.params.char_val_by_uuid_read_rsp.handle_value[i]);
                nrf_delay_ms(2);
            }
        }

        case BLE_GATTC_EVT_TIMEOUT:
        {
            printf("GATTC timeout\r\n");
            // Disconnect on GATT Client timeout event.
            // NRF_LOG_DEBUG("GATT Client Timeout.");
            // err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
            //                                  BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            // APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_WRITE_RSP:
        {
            printf("WRITE_RSP\r\n");

            Device.BLE_Complete();
        } break;

    #if 0
        case BLE_GAP_EVT_ADV_REPORT:
            cb_adv_report(&p_gap_evt->params.adv_report);
            break;

        case BLE_GATTC_EVT_ATTR_INFO_DISC_RSP:
        {
            printf("ATTRINFO Response len: %u (%u, %u):\r\n", p_gattc_evt->params.attr_info_disc_rsp.count, p_gattc_evt->gatt_status, p_gattc_evt->error_handle);

            for(uint16_t i = 0; i < p_gattc_evt->params.attr_info_disc_rsp.count; ++i) {
                printf("\tHandle: 0x%04X, UUID: 0x%04X\r\n", p_gattc_evt->params.attr_info_disc_rsp.info.attr_info16[i].handle, p_gattc_evt->params.attr_info_disc_rsp.info.attr_info16[i].uuid.uuid);
                nrf_delay_ms(2);
            }

            Device.BLE_Complete();
        } break;
    #endif

        case BLE_GAP_EVT_TIMEOUT:
        {
            Device.BLE_Complete();

            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
            {
                NRF_LOG_DEBUG("Connection Request timed out.");
            }
        } break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
        {
            // Accepting parameters requested by peer.
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                                    &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTS_EVT_TIMEOUT:
        {
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
        } break;

        #if 0
        case BLE_GATTC_EVT_EXCHANGE_MTU_RSP:
        {
            // APP_ERROR_CHECK( sd_ble_gatts_exchange_mtu_reply(p_ble_evt->evt.gattc_evt.conn_handle, NRF_SDH_BLE_GATT_MAX_MTU_SIZE) );
            /* @todo implement this */
        } break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
        {
            /* @todo implement this */
        } break;
        #endif

        default:
            break;
    }
}

static void cb_pm_event(pm_evt_t const * p_evt)
{
    pm_handler_on_pm_evt(p_evt);
    pm_handler_disconnect_on_sec_failure(p_evt);
    pm_handler_flash_clean(p_evt);

    switch (p_evt->evt_id)
    {
        case PM_EVT_PEERS_DELETE_SUCCEEDED:
            scan_start();
            break;

        default:
            break;
    }
}

#if 0
static const char* scan_events[] = {
    "NRF_BLE_SCAN_EVT_FILTER_MATCH",         /**< A filter is matched or all filters are matched in the multifilter mode. */
    "NRF_BLE_SCAN_EVT_WHITELIST_REQUEST",    /**< Request the whitelist from the main application. For whitelist scanning to work, the whitelist must be set when this event occurs. */
    "NRF_BLE_SCAN_EVT_WHITELIST_ADV_REPORT", /**< Send notification to the main application when a device from the whitelist is found. */
    "NRF_BLE_SCAN_EVT_NOT_FOUND",            /**< The filter was not matched for the scan data. */
    "NRF_BLE_SCAN_EVT_SCAN_TIMEOUT",         /**< Scan timeout. */
    "NRF_BLE_SCAN_EVT_CONNECTING_ERROR",     /**< Error occurred when establishing the connection. In this event, an error is passed from the function call @ref sd_ble_gap_connect. */
    "NRF_BLE_SCAN_EVT_CONNECTED"   
};
#endif

static void cb_scan_event(scan_evt_t const * p_scan_evt)
{
    switch( p_scan_evt->scan_evt_id ) {

        case NRF_BLE_SCAN_EVT_NOT_FOUND:
            /* No filter was set, so an advertisement packet will lead to this
             * event. */
            cb_adv_report(p_scan_evt->params.p_not_found);
            break;

        case NRF_BLE_SCAN_EVT_SCAN_TIMEOUT:
            /* Scanning completed. Tell the application to parse the data. */
            Device.BLE_Complete();
            break;

        default:
            /* No implementation needed. */
            break;
    }

#if 0
    ret_code_t err_code;
    // printf("Scan event: %u\r\n", p_scan_evt->scan_evt_id);
    switch(p_scan_evt->scan_evt_id)
    {
        case NRF_BLE_SCAN_EVT_NOT_FOUND:
            /* And advertisement report has been received */
            // cb_adv_report(p_scan_evt->params.p_not_found);



            (void)scan_event_complete;
            // scan_event_complete(p_scan_evt->params.p_not_found);
            break;

        case NRF_BLE_SCAN_EVT_SCAN_TIMEOUT:
            break;

        case NRF_BLE_SCAN_EVT_WHITELIST_REQUEST:
        {
            // printf("NRF_BLE_SCAN_EVT_WHITELIST_REQUEST\r\n");
            whitelist_request();
            m_whitelist_disabled = false;
        } break;

        case NRF_BLE_SCAN_EVT_CONNECTING_ERROR:
        {
            // printf("NRF_BLE_SCAN_EVT_CONNECTING_ERROR\r\n");
            err_code = p_scan_evt->params.connecting_err.err_code;
            APP_ERROR_CHECK(err_code);
        } break;



        case NRF_BLE_SCAN_EVT_FILTER_MATCH:
            // printf("NRF_BLE_SCAN_EVT_FILTER_MATCH\r\n");
            break;
        case NRF_BLE_SCAN_EVT_WHITELIST_ADV_REPORT:
            // printf("NRF_BLE_SCAN_EVT_WHITELIST_ADV_REPORT\r\n");
            break;

        default:
          break;
    }
#endif
}

static void cb_soc_event(uint32_t evt_id, void * p_context)
{
    switch (evt_id)
    {
        case NRF_EVT_FLASH_OPERATION_SUCCESS:
        /* fall through */
        case NRF_EVT_FLASH_OPERATION_ERROR:

            if (m_memory_access_in_progress)
            {
                m_memory_access_in_progress = false;
                scan_start();
            }
            break;

        default:
            // No implementation needed.
            break;
    }
}

static void clear_characteristics(BLE_Service_t* const p_service)
{
    while( p_service->characteristic ) {
        /* Delete node from linked list */
        BLE_Char_t* p_next = p_service->characteristic->next;
        free(p_service->characteristic);
        p_service->characteristic = p_next;
    }

    /* Reset counter */
    p_service->char_count = 0;
}

static void clear_services(void)
{
    while( manager.service ) {
        /* Clear any characteristics first */
        clear_characteristics(manager.service);

        /* Delete node from linked list */
        BLE_Service_t* p_next = manager.service->next;
        free(manager.service);
        manager.service = p_next;
    }

    /* Reset counter */
    manager.service_count = 0;
}

static uint32_t gatt_init(void)
{
    RET_ON_ERROR( nrf_ble_gatt_init(&m_gatt, NULL) );
    RET_ON_ERROR( nrf_ble_gatt_att_mtu_central_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE) );

    return 0;
}

static uint32_t get_char_handle_from_UUID(const uint16_t char_uuid, const BLE_Service_t* p_service, BLE_Char_t** const pp_characteristic)
{
    uint32_t err = DEV_ERROR_BLE_PARAM;
    BLE_Char_t* p_characteristic = p_service->characteristic;

    while( p_characteristic ) {

        if( p_characteristic->gattc_char.uuid.uuid == char_uuid ) {
            *pp_characteristic = p_characteristic;
            err = 0;
            break;
        }

        p_characteristic = p_characteristic->next;
    }

    return err;
}

static uint32_t get_char_UUID_from_value_handle(const uint16_t value_handle, uint16_t* const p_serv_uuid, uint16_t* const p_char_uuid)
{
    uint32_t err = DEV_ERROR_BLE_PARAM;

    BLE_Service_t* p_service = manager.service;
    while( p_service ) {
        BLE_Char_t* p_characteristic = p_service->characteristic;

        while( p_characteristic ) {
            if( p_characteristic->gattc_char.handle_value == value_handle ) {
                *p_serv_uuid = p_service->gattc_service.uuid.uuid;
                *p_char_uuid = p_characteristic->gattc_char.uuid.uuid;
                err = 0;
                break;
            }

            p_characteristic = p_characteristic->next;
        }

        if( !err ) {
            break;
        }

        p_service = p_service->next;
    }

    return err;
}

static uint32_t get_service_handle_from_UUID(const uint16_t serv_handle, BLE_Service_t** const pService)
{
    uint32_t err = DEV_ERROR_BLE_PARAM;
    BLE_Service_t* serv = manager.service;

    while( serv ) {

        if( serv->gattc_service.uuid.uuid == serv_handle ) {
            *pService = serv;
            err = 0;
            break;
        }

        serv = serv->next;
    }

    return err;
}

#if 0
static void peer_list_get(pm_peer_id_t * p_peers, uint32_t * p_size)
{
    pm_peer_id_t peer_id;
    uint32_t     peers_to_copy;

    peers_to_copy = (*p_size < BLE_GAP_WHITELIST_ADDR_MAX_COUNT) ?
                     *p_size : BLE_GAP_WHITELIST_ADDR_MAX_COUNT;

    peer_id = pm_next_peer_id_get(PM_PEER_ID_INVALID);
    *p_size = 0;

    while ((peer_id != PM_PEER_ID_INVALID) && (peers_to_copy--))
    {
        p_peers[(*p_size)++] = peer_id;
        peer_id              = pm_next_peer_id_get(peer_id);
    }
}
#endif

static uint32_t peer_manager_init(void)
{
    RET_ON_ERROR( pm_init() );

    ble_gap_sec_params_t sec_param = {0};

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    RET_ON_ERROR( pm_sec_params_set(&sec_param) );

    RET_ON_ERROR( pm_register(cb_pm_event) );
    
    return 0;
}

#if 0 
static void scan_event_complete(const ble_gap_evt_adv_report_t* const pReport)
{
    // if( pReport->type.scan_response ) {
    //     printf("Response\r\n");
    // }

    /* Iterate over device list to see if a new device was detected. */
    for( uint8_t i = 0; i < SCAN_MAX_DEVICES; ++i ) {

        // Parse the long address from the scan event
        uint64_t l_addr = 0;
        memcpy((uint8_t*) &l_addr, pReport->peer_addr.addr, BLE_GAP_ADDR_LEN);

        /* Add the device if the list was traversed without a match */
        if( scanned_devices[i] == 0 ) {
            scanned_devices[i] = (ScanData_t*) malloc(sizeof(ScanData_t));

            if( !scanned_devices[i] )   APP_ERROR_CHECK(NRF_ERROR_NO_MEM);

            memcpy(scanned_devices[i]->addr, pReport->peer_addr.addr, BLE_GAP_ADDR_LEN);
            scanned_devices[i]->l_addr = l_addr;

            // if( scanned_devices[i]->datalen < 16 ) {
            //     scanned_devices[i]->data[scanned_devices[i]->datalen].len = pReport->data.len;
            //     scanned_devices[i]->data[scanned_devices[i]->datalen].p_data = (uint8_t*) malloc(pReport->data.len);
            //     memcpy(scanned_devices[i]->data[scanned_devices[i]->datalen].p_data, pReport->data.p_data, pReport->data.len);
            //     scanned_devices[i]->datalen++;
            // }

            n_scanned_devices++;
            break;
        }
        /* We have a match! */
        else if( scanned_devices[i]->l_addr == l_addr ) {
            // if( scanned_devices[i]->datalen < 16 ) {
            //     scanned_devices[i]->data[scanned_devices[i]->datalen].len = pReport->data.len;
            //     scanned_devices[i]->data[scanned_devices[i]->datalen].p_data = (uint8_t*) malloc(pReport->data.len);
            //     memcpy(scanned_devices[i]->data[scanned_devices[i]->datalen].p_data, pReport->data.p_data, pReport->data.len);
            //     scanned_devices[i]->datalen++;
            // }
            break;
        }
    }
}
#endif

static uint32_t scan_filters_set(void)
{
    ble_uuid_t target_uuid = {.uuid = TARGET_UUID, .type = BLE_UUID_TYPE_BLE};

    RET_ON_ERROR( nrf_ble_scan_filter_set(&m_scan, SCAN_NAME_FILTER, m_target_periph_name) );

    RET_ON_ERROR( nrf_ble_scan_filter_set(&m_scan, SCAN_UUID_FILTER, &target_uuid) );

    if (is_connect_per_addr)
    {
        RET_ON_ERROR( nrf_ble_scan_filter_set(&m_scan, SCAN_ADDR_FILTER, m_target_periph_addr.addr) );
        // RET_ON_ERROR( nrf_ble_scan_filters_enable(&m_scan,
        //                NRF_BLE_SCAN_NAME_FILTER | NRF_BLE_SCAN_UUID_FILTER | NRF_BLE_SCAN_ADDR_FILTER,
        //                false) );
    }
    else
    {
        // RET_ON_ERROR( nrf_ble_scan_filters_enable(&m_scan,
        //                NRF_BLE_SCAN_NAME_FILTER | NRF_BLE_SCAN_UUID_FILTER,
        //                false) );
    }

    return 0;
}

static uint32_t scan_init(void)
{
    RET_ON_ERROR( nrf_ble_scan_init(&m_scan, NULL, cb_scan_event) );
    RET_ON_ERROR( nrf_ble_scan_params_set(&m_scan, &m_scan_param) );

    return 0;
}

// #include <inttypes.h>
static uint32_t scan_start(void)
{
    /* Free already allocated memory from device list */
    for( uint8_t i = 0; i < SCAN_MAX_DEVICES; ++i ) {
        
        if( scanned_devices[i] ) {
            free(scanned_devices[i]->adv_data.data.p_data);
            scanned_devices[i]->adv_data.data.p_data = 0;
            free(scanned_devices[i]->sr_data.data.p_data);
            scanned_devices[i]->sr_data.data.p_data = 0;
        }

        free(scanned_devices[i]);
        scanned_devices[i] = 0;
    }

    n_scanned_devices = 0;

    

/*    for(uint8_t i = 0; i < SCAN_MAX_DEVICES; ++i) {

        // if(scanned_devices[i]) {
        //     for(uint16_t j = 0; j < scanned_devices[i]->datalen; ++j) {
        //         free(scanned_devices[i]->data[j].p_data);

        //         printf("freeing %u\r\n", j);
        //         nrf_delay_ms(10);
        //     }
        // }
        if( scanned_devices[i])
            free(scanned_devices[i]->data.p_data);
        
        free(scanned_devices[i]);
        scanned_devices[i] = 0;
    }*/

    // If there is any pending write to flash, defer scanning until it completes.
    if (nrf_fstorage_is_busy(NULL))
    {
        m_memory_access_in_progress = true;
        return 0;
    }

    // RET_ON_ERROR( nrf_ble_scan_params_set(&m_scan, &m_scan_param) );
    // (void)m_scan_param;

    // RET_ON_ERROR( nrf_ble_scan_filters_disable(&m_scan) );
    // RET_ON_ERROR( nrf_ble_scan_all_filter_remove(&m_scan) );

    RET_ON_ERROR( nrf_ble_scan_start(&m_scan) );

    // sd_ble_gap_scan_start();
    
    return 0;
}

#if 0

static uint32_t whitelist_load(void)
{
    ret_code_t   ret;
    pm_peer_id_t peers[8];
    uint32_t     peer_cnt;

    memset(peers, PM_PEER_ID_INVALID, sizeof(peers));
    peer_cnt = (sizeof(peers) / sizeof(pm_peer_id_t));

    // Load all peers from flash and whitelist them.
    peer_list_get(peers, &peer_cnt);

    RET_ON_ERROR( pm_whitelist_set(peers, peer_cnt) );

    // Setup the device identities list.
    // Some SoftDevices do not support this feature.
    ret = pm_device_identities_list_set(peers, peer_cnt);
    if (ret != NRF_ERROR_NOT_SUPPORTED) {
        return ret;
    }

    return ret;
}

static void whitelist_request(void)
{
    // Whitelist buffers.
    ble_gap_addr_t whitelist_addrs[8];
    ble_gap_irk_t  whitelist_irks[8];

    memset(whitelist_addrs, 0x00, sizeof(whitelist_addrs));
    memset(whitelist_irks, 0x00, sizeof(whitelist_irks));

    uint32_t addr_cnt = (sizeof(whitelist_addrs) / sizeof(ble_gap_addr_t));
    uint32_t irk_cnt  = (sizeof(whitelist_irks) / sizeof(ble_gap_irk_t));

    // Reload the whitelist and whitelist all peers.
    whitelist_load();

    ret_code_t err_code;

    // Get the whitelist previously set using pm_whitelist_set().
    err_code = pm_whitelist_get(whitelist_addrs, &addr_cnt,
                                whitelist_irks, &irk_cnt);

    if (((addr_cnt == 0) && (irk_cnt == 0)) ||
        (m_whitelist_disabled))
    {
        // Don't use whitelist.
        err_code = nrf_ble_scan_params_set(&m_scan, NULL);
        APP_ERROR_CHECK(err_code);
    }

    NRF_LOG_INFO("Starting scan.");

    // err_code = bsp_indication_set(BSP_INDICATE_SCANNING);
    // APP_ERROR_CHECK(err_code);
}
#endif
