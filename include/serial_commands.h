/**
 * @file serial_commands.h
 * @brief      BLE Bridge serial command definitions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

#ifndef __SERIAL_COMMANDS_H
#define __SERIAL_COMMANDS_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/**
 * @addtogroup   BLEBridge_Serial
 * @{
 */

/**
 * @defgroup   Serial_Commands BLEBridge Serial Commands
 * @{
 */

/* Exported constants --------------------------------------------------------*/

#define COMMAND_FAM_BLE                         0x0A
#define COMMAND_IDX_SCAN                        0x01
#define COMMAND_IDX_GET_SCANNED                 0x02
#define COMMAND_IDX_GET_DEVICE                  0x03
#define COMMAND_IDX_CONNECT                     0x10

#define COMMAND_FAM_SERV                        0x0B
#define COMMAND_IDX_DISCOVER                    0x01
#define COMMAND_IDX_GET_DISC                    0x02

#define COMMAND_FAM_CHAR                        0x0C
// #define COMMAND_IDX_DISCOVER                    0x01
// #define COMMAND_IDX_GET_DISC                    0x02
#define COMMAND_IDX_READ                        0x03
#define COMMAND_IDX_WRITE                       0x04
#define COMMAND_IDX_SUBSCRIBE                   0x05

#define COMMAND_FAM_DEVICE                      0xF0
#define COMMAND_IDX_STATUS                      0x01

/* Exported types ------------------------------------------------------------*/

/* Exported macros -----------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      Parse a command belonging to the BLE command family.
 *
 * @param[in]  args       Pointer to the arguments of this command.
 * @param[in]  arglen     The length in bytes of @p args.
 * @param[out] p_err      Pointer to the error to be returned to the user.
 * @param[out] p_confirm  Pointer whether confirmation should be issued or not.
 *
 * @return     an error from #BLEBridge_Errors.
 */
uint32_t Command_Parse_BLE(const uint8_t* args, const uint16_t arglen, uint8_t* const p_err, bool* const p_confirm);

/**
 * @brief      Parse a command belonging to the characteristic command family.
 *
 * @param[in]  args       Pointer to the arguments of this command.
 * @param[in]  arglen     The length in bytes of @p args.
 * @param[out] p_err      Pointer to the error to be returned to the user.
 * @param[out] p_confirm  Pointer whether confirmation should be issued or not.
 *
 * @return     an error from #BLEBridge_Errors.
 */
uint32_t Command_Parse_CHAR(const uint8_t* args, const uint16_t arglen, uint8_t* const p_err, bool* const p_confirm);

/**
 * @brief      Parse a command belonging to the device command family.
 *
 * @param[in]  args       Pointer to the arguments of this command.
 * @param[in]  arglen     The length in bytes of @p args.
 * @param[out] p_err      Pointer to the error to be returned to the user.
 * @param[out] p_confirm  Pointer whether confirmation should be issued or not.
 *
 * @return     an error from #BLEBridge_Errors.
 */
uint32_t Command_Parse_DEVICE(const uint8_t* args, const uint16_t arglen, uint8_t* const p_err, bool* const p_confirm);

/**
 * @brief      Parse a command belonging to the service command family.
 *
 * @param[in]  args       Pointer to the arguments of this command.
 * @param[in]  arglen     The length in bytes of @p args.
 * @param[out] p_err      Pointer to the error to be returned to the user.
 * @param[out] p_confirm  Pointer whether confirmation should be issued or not.
 *
 * @return     an error from #BLEBridge_Errors.
 */
uint32_t Command_Parse_SERV(const uint8_t* args, const uint16_t arglen, uint8_t* const p_err, bool* const p_confirm);

/**
 * @}
 */

/**
 * @}
 */

#endif /* __SERIAL_COMMANDS_H included */
