/**
 * @file serial.h
 * @brief      BLE Bridge serial interface definitions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

#ifndef __SERIAL_H
#define __SERIAL_H

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>

/**
 * @addtogroup BLEBridge
 * @{
 */

/**
 * @defgroup   BLEBridge_Serial BLE Bridge Serial Module
 * @{
 */

/* Exported constants --------------------------------------------------------*/

/*! Notification command character ('!') for the command transmission */
#define SERIAL_CMD_NOTIF                        0x21
/*! Setter command character ('<') for the command transmission */
#define SERIAL_CMD_GETTER                       0x3C
/*! Setter command character ('>') for the command transmission */
#define SERIAL_CMD_SETTER                       0x3E
/*! Termination character for the command transmission */
#define SERIAL_CMD_TERMINATOR                   0xFA

/*! Helper macro to write the response */
#define WRITE_RESPONSE( data, ... )             printf("%c" data "%c\r\n", SERIAL_CMD_GETTER, ##__VA_ARGS__, SERIAL_CMD_TERMINATOR)
/*! Helper macro to write data to the interface */
#define WRITE_DATA( data )                      
/*! Helper macro to return an error */
#define WRITE_ERROR( err )                      WRITE_RESPONSE("ERR=%02X", err)
/*! Write a notification to the interface */
#define WRITE_NOTIFICATION( data, ... )         printf("%c" data "%c\r\n", SERIAL_CMD_NOTIF, ##__VA_ARGS__, SERIAL_CMD_TERMINATOR)
/*! Helper macro to return ok */
#define WRITE_OK( )                             WRITE_RESPONSE("OK+%02X", 0)

/* Exported types ------------------------------------------------------------*/

/* Exported macros -----------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      Encodes a data buffer into an ascii bytestring.
 *
 * @param[in]  p_data  Pointer to the data to be encoded.
 * @param[in]  size    The number of bytes to encode.
 * @param[out] p_buf   Pointer to a buffer with at least 2* @p size + 1 bytes of
 *                     allocated memory.
 * @param[out] offs    The offset in @p p_buf to start writing to. It contains
 *                     the total number of bytes written after the call to this
 *                     function.
 */
void Serial_Encode(const uint8_t* p_data, const uint32_t size, uint8_t* p_buf, uint32_t* const offs);

/**
 * @brief      Writes '\r\n' to the buffer in order to flush it.
 */
void Serial_Flush(void);

/**
 * @brief      Initialises the serial interface of the dongle.
 *
 * @return     any error that comes up during initialisation.
 */
uint32_t Serial_Init(void);

/**
 * @brief      Processes any serial event, that might have come up.
 *
 * @param[out] pending  The pending flag that will be or-ed with it's initial
 *                      value.
 *
 * @return     any error that comes up during processing.
 */
uint32_t Serial_Process(bool* const pending);

/**
 * @brief      Writes data to the serial interface.
 *
 * @param[in]  p_data  Pointer to the data to be written.
 * @param[in]  size    The number of bytes to be written.
 *
 * @return     any error that comes up during processing.
 */
uint32_t Serial_Write(const uint8_t* p_data, const uint32_t size);

/**
 * @}
 */

/**
 * @}
 */

#endif /* __SERIAL_H included */
