/**
 * @file bridge_ble.h
 * @brief      BLE Bridge stack definitions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

#ifndef __BRIDGE_BLE_H
#define __BRIDGE_BLE_H

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>
#include <ble_gap.h>

/**
 * @addtogroup   BLEBridge
 * @{
 */

/**
 * @defgroup   BLEBridge_Stack BLE Bridge BLE Stack definitions
 * @{
 */

/* Exported constants --------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

typedef union {
    ble_gap_adv_report_type_t   bitfield;
    uint16_t                    value;
} adv_report_type_t;

typedef struct {
    adv_report_type_t   type;
    ble_data_t          data;
} adv_data_t;

typedef struct {

    ble_gap_addr_t  peer_addr_new;

    /* @todo remove union */
    union {
        uint8_t     pAddr[8];
        uint64_t    lAddr;
    } peer_addr;

    adv_data_t  adv_data;
    adv_data_t  sr_data;
    bool        sr_data_valid;
} ScanData_t;

/* Exported macros -----------------------------------------------------------*/

/* Exported variables --------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      Discovers the characteristics of a given service UUID.
 *
 * @param[in]  serv_uuid  The UUID of the service.
 *
 * @return     any error that comes up.
 */
uint32_t BLE_CHAR_Discover(const uint16_t serv_uuid);

uint32_t BLE_CHAR_LongWriteExec(const uint16_t serv_uuid, const uint16_t char_uuid);
uint32_t BLE_CHAR_LongWritePrepare(const uint16_t serv_uuid, const uint16_t char_uuid, const uint8_t* p_data, const uint16_t size, const uint16_t offs);

/**
 * @brief      Reads the characteristic value by service and char UUIDs.
 *
 * @param[in]  serv_uuid  The service UUID.
 * @param[in]  char_uuid  The characteristic UUID.
 * @param[out] p_data     Pointer to store the memory address of the result.
 * @param[out] p_size     Pointer to a two-byte array to store the length of the
 *                        read operation.
 *
 * @return     any error that comes up.
 *
 * @note       The output parameters are only valid, once the request was
 *             completed by the SoftDevice. This is not necessarily the case
 *             when the function returns.
 *
 * @note       * @p p_data points to a memory region with size * @p size on the
 *             heap. It must be freed by the user after it is no longer used.
 */
uint32_t BLE_CHAR_Read(const uint16_t serv_uuid, const uint16_t char_uuid, uint8_t** p_data, uint16_t* const p_size);

/**
 * @brief      Subscribe to indications or notifications of a given
 *             characteristic UUID.
 *
 * @param[in]  serv_uuid  The service UUID of the characteristic.
 * @param[in]  char_uuid  The UUID of the characteristic.
 * @param[in]  subs       Flag indicating if indications, notifications or none
 *                        should be used.
 *
 * @return     any error that comes up.
 */
uint32_t BLE_CHAR_Subscribe(const uint16_t serv_uuid, const uint16_t char_uuid, const uint8_t subs);

/**
 * @brief      Writes data to a given characteristic UUID.
 *
 * @param[in]  serv_uuid  The service UUID of the characteristic.
 * @param[in]  char_uuid  The UUID of the characteristic.
 * @param[in]  p_data     Pointer to the data that is to be written.
 * @param[in]  size       The number of bytes in @p p_data.
 *
 * @return     any error that comes up.
 */
uint32_t BLE_CHAR_Write(const uint16_t serv_uuid, const uint16_t char_uuid, const uint8_t* p_data, const uint16_t size);

/**
 * @brief      Connect to a device using it's BLE address.
 *
 * @param[in]  p_addr  Pointer to the BLE address of the peripheral.
 *
 * @return     any error that comes up.
 */
uint32_t BLE_Connect_Address(const uint8_t* p_addr);

/**
 * @brief      Connect to a device using the index in the internal scan list.
 *
 * @param[in]  index  The index of the peripheral.
 *
 * @return     any error that comes up.
 */
uint32_t BLE_Connect_Index(const uint8_t index);

/**
 * @brief      Runs a descriptor discovery process under a given characteristic.
 *
 * @param[in]  char_uuid  The UUID of the characteristic.
 *
 * @return     any error that comes up.
 */
uint32_t BLE_DESC_Discover(const uint16_t char_uuid);

/**
 * @brief      Retrieve the characteristic UUIDs discovered under a given
 *             service UUID.
 *
 * @param[in]  serv_uuid  The UUID of the service.
 * @param[out] p_buf      Pointer to a buffer to store the characteristic UUIDs.
 * @param[in]  size       The number of 16-bit values that can be stored in @p
 *                        p_buf.
 */
void BLE_GetChars(const uint16_t serv_uuid, uint16_t* p_buf, const uint16_t size);

/**
 * @brief      Get the number of characteristics discovered under a given
 *             service UUID.
 *
 * @param[in]  serv_uuid  The UUID of the service.
 *
 * @return     the number of characteristics.
 */
uint16_t BLE_GetNumberOfCharacteristics(const uint16_t serv_uuid);

/**
 * @brief      Get the number of devices in the scan list.
 *
 * @return     the number of devices in the scan list.
 */
uint8_t BLE_GetNumberOfScannedDevices(void);

/**
 * @brief      Get the number of services discovered from the connected device.
 *
 * @return     the number of services.
 */
uint16_t BLE_GetNumberOfServices(void);

/**
 * @brief      Returns the scan data of a device in the scan list.
 *
 * @param[in]  index  The index in the scan list.
 *
 * @return     read-only pointer to the device.
 */
const ScanData_t* const BLE_GetScannedDevice(const uint8_t index);

/**
 * @brief      Retrieve the service UUIDs discovered from the connected device.
 *
 * @param[out] p_buf  Pointer to a buffer to store the characteristic UUIDs.
 * @param[in]  size   The number of 16-bit values that can be stored in @p
 *                    p_buf.
 */
void BLE_GetServices(uint16_t* p_buf, const uint16_t size);

/**
 * @brief      Initialises the BLE stack.
 *
 * @return     any error that comes up.
 */
uint32_t BLE_Init(void);

/**
 * @brief      Initiates a scan procedure.
 *
 * @return     any error that comes up.
 */
uint32_t BLE_Scan(void);

/**
 * @brief      Discovers the services on the connected device.
 *
 * @param[in]  start_handle  The start handle to scan from.
 *
 * @return     any error that comes up.
 */
uint32_t BLE_SERV_Discover(const uint16_t start_handle);

/**
 * @}
 */

/**
 * @}
 */

#endif /* __BRIDGE_BLE_H included */
