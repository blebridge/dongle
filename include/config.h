/**
 * @file config.h
 * @brief      BLE Bridge dongle configuration.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

#ifndef CUSTOM_CONFIG_H
#define CUSTOM_CONFIG_H

#define FDS_ENABLED 1

#define GPIOTE_ENABLED 1

#define GPIOTE_CONFIG_NUM_OF_LOW_POWER_EVENTS 4

#define GPIOTE_CONFIG_IRQ_PRIORITY 6

#define NRF_BLE_SCAN_SCAN_INTERVAL 1600

#define NRF_BLE_SCAN_NAME_CNT 1

#define NRF_FSTORAGE_ENABLED 1

#define NRF_LOG_BACKEND_RTT_ENABLED 0

#define NRFX_UARTE0_ENABLED 1

#define PEER_MANAGER_ENABLED 1

#endif //CUSTOM_CONFIG_H