/**
 * @file device.h
 * @brief      BLE Bridge dongle device definitions.
 *
 * @author     Yvan Bosshard, byvan@ethz.ch
 * @date       January 2021
 */

#ifndef __DEVICE_H
#define __DEVICE_H

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>

/**
 * @addtogroup   BLEBridge
 * @{
 */

/**
 * @defgroup   BLEBridge_Device BLE Bridge Device Module
 * @{
 */

/* Exported constants --------------------------------------------------------*/

/**
 * @defgroup   BLEBridge_Errors BLE Bridge Errors
 * @{
 */

#define ERR_DEVICE_BUSY             0x01
#define ERR_DEVICE_NOT_CONNECTED    0x02
#define ERR_INVALID_PARAM           0xFD
#define ERR_UNSUPPORTED             0xFE
#define ERR_COMMAND_USER            0xFF

/**
 * @}
 */

#define DEV_STATUS_BLEBUSY      0
#define DEV_STATUS_CONNECTED    1
#define DEV_STATUS_SCAN         2
#define DEV_STATUS_RES3         3
#define DEV_STATUS_RES4         4
#define DEV_STATUS_RES5         5
#define DEV_STATUS_RES6         6
#define DEV_STATUS_RES7         7

#define DEV_ERROR_BLE_PARAM     0xA0

/* Exported types ------------------------------------------------------------*/

/**
 * @brief      Operation enumerator.
 */
typedef enum {
    OP_NONE         = 0,    /*!< No operation pending */
    OP_BLE_SCAN,            /*!< Scan for BLE devices */
    OP_BLE_GET_SCANNED,     /*!< Write the list of scanned devices to the interface */
    OP_BLE_GET_DEVICE,      /*!< Write device info to the interface */
    OP_BLE_CONNECT_DEVICE,  /*!< Connect to a device with given index */
    OP_SERV_DISCOVER,       /*!< Discover services */
    OP_SERV_GET,            /*!< List the available services */
    OP_CHAR_DISCOVER,       /*!< Discover characteristics */
    OP_CHAR_GET,            /*!< Get discovered characteristics */
    OP_CHAR_READ,           /*!< Read a characteristic value */
    OP_CHAR_WRITE,          /*!< Write a characteristic value */
    OP_CHAR_SUBSCRIBE,      /*!< Subscribe to a characteristic value */
    OP_CHAR_NOTIFICATION,   /*!< Process a HVX notification */
} DeviceOperationType_t;

/**
 * @brief      Struct to access the internal device functionality.
 */
typedef struct {
    void        (*BLE_Complete)(void);                              /*!< Notification to indicate completion of a BLE task */
    void        (*BLE_ScanComplete)(const uint16_t handle);         /*!< Notification to indicate scan complete */
    void        (*Connected)(void);                                 /*!< Notification about successful connection */
    void        (*Disconnected)(void);                              /*!< Notification about disconnect event  */
    uint8_t     (*GetNumberOfDevices)(void);                        /*!< Pointer to access the number of devices detected */
    uint8_t     (*GetStatus)(void);                                 /*!< Pointer to the access function of the device status register */
    uint8_t     (*Schedule)(DeviceOperationType_t op,
                            void* args,
                            uint16_t arglen);                       /*!< Pointer to the access function of the device operation scheduler */
} DeviceHandler_t;

/* Exported macros -----------------------------------------------------------*/

/* Exported variables --------------------------------------------------------*/

/*! Device access handler */
extern DeviceHandler_t Device;

/* Exported functions --------------------------------------------------------*/

/**
 * @brief      Initialises the main device functionality.
 *
 * @return     any error that comes up.
 */
uint32_t Device_Init(void);

/**
 * @brief      Processes an event in the operation queue.
 *
 * @param      pending  Flag to indicate if operations are pending.
 *
 * @return     any error that comes up.
 */
uint32_t Device_Process(bool* const pending);

/**
 * @brief      Signals the serial interface the ready state.
 */
void Device_Ready(void);

/**
 * @}
 */

/**
 * @}
 */

#endif /* __DEVICE_H included */
